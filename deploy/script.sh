#!/bin/bash

sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get install curl -y

curl -fsSL https://get.docker.com -o get-docker.sh 
sudo sh get-docker.sh

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash

sudo apt-get install gitlab-runner

# Simulate pressing Enter key
echo -ne '\n'
# Simulate pressing Enter key
echo -ne '\n'
# Simulate pressing Enter key
echo -ne '\n'
# Simulate pressing Enter key
echo -ne '\n'
